package temperature;

import java.util.Scanner;

/**
 * Класс для расчета общей, средней, максимальной, минимальной температуры
 * Дней с максимальной и минимальной температурой
 * @author Mashina A.A
 */

 class temperature {
    private static Scanner scanner = new Scanner(System.in);

     public static void main(String[] args) {
        int numberOfDays = readNumberOfDays();
        double temperature[] = new double[numberOfDays];

        fillTemperature(temperature);
        double minT=minTemperature(temperature);
        double maxT=maxTemperature(temperature);

        System.out.printf("%5.1f", averageTemperature(temperature));
        System.out.printf("%nМинимальная температура = %.1f", minT);
        System.out.printf("%nМаксимальная температура = %.1f", maxT);

        System.out.printf("%nДни с максимальной температорой: ");
        outArray (maxDaysTemperature(temperature,maxT));
        System.out.printf("%nДни с минимальной температорой: ");
        outArray(minDaysTemperature(temperature,minT));

    }

    /**
     * Возвращает количество дней в месяце если они их не меньше 28
     * @return количество дней
     */
    private static int readNumberOfDays() {
        System.out.print("Введите кол-во дней: ");
        int numberOfDays = scanner.nextInt();
        while(numberOfDays < 28 && numberOfDays>32) {
            System.out.printf("Вы ввели некорректные данные. В месяце больше чем %d дней.%n", numberOfDays);
            System.out.print("Введите кол-во дней: ");
            numberOfDays = scanner.nextInt();
        }
        return numberOfDays;
    }

    /**
     * Заполняет значение температуры
     * @param temperature массив температуры в месяце
     */
    private static void fillTemperature(double temperature[]) {
        for (int i = 0; i < temperature.length; i++) {
            //          temperature[i] = scanner.nextDouble();
            temperature[i] = -10 + (Math.random() * 30);
            System.out.printf("%1.1f ", temperature[i]);
        }
    }

    /**
     * Находит общую и среднюю температуру за месяц
     * @param temperature массив температуры в месяце
     * @return среднюю температуру
     */
    private static double averageTemperature(double temperature[]) {
        double sum = 0;
        for (double temperatureOfDay : temperature) {
            sum = sum + temperatureOfDay;
        }
        System.out.printf("%6.1f", sum);
        return sum / temperature.length;
    }

    /**
     * Находит минимальную температуру
     * @param temperature массив температуры в месяце
     * @return минимальное значение температуры
     */
    private static double minTemperature(double temperature[]) {
        double min = temperature[0];
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] < min) {
                min = temperature[i];
                int day = i;
            }
        }
        return min;
    }

    /**
     * Находит количество дней с минимальной температурой
     * @param temperature массив температуры в месяце
     * @return количество дней
     */
    private static int minDayTemperature(double temperature[]) {
        int day = 0;
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] == minTemperature(temperature)) {
                day++;
            }
        }
        return day;
    }

    /**
     * Создает массив с днями с минимальной температурой
     * @param temperature массив температуры в месяце
     * @param minT минимальная температура
     * @return массив с днями
     */
    private static int[] minDaysTemperature(double[] temperature, double minT) {
        int[] day = new int[minDayTemperature(temperature)];
        int j=-1;
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] == minT) {
                j++;
                day[j] = i + 1;
            }
        }
        return day;
    }

    /**
     * Находит день с максимальной температурой
     * @param temperature массив температуры в месяце
     * @return количество дней
     */
    private static double maxTemperature(double temperature[]) {
        double max = temperature[0];
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] > max) {
                max = temperature[i];
            }
        }
        return max;
    }

    /**
     * Находит количество дней с максимальной температурой
     * @param temperature массив температуры в месяце
     * @return максимальное значение температуры
     */
    private static int maxDayTemperature(double temperature[]) {
        int day = 0;
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] == maxTemperature(temperature)) {
                day++;
            }
        }
        return day;
    }

    /**
     * Создает массив с днями с максимальной температурой
     * @param temperature массив температуры в месяце
     * @param maxT максимальная температура
     * @return массив с днями
     */
    private static int[] maxDaysTemperature(double[] temperature,double maxT) {
        int[] day = new int[maxDayTemperature(temperature)];
        int j=-1;
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] == maxT) {
                j++;
                day[j] = i + 1;
            }
        }
        return day;
    }

    /**
     * Выводит элементы массива
     * @param days массив
     */
    private static void outArray (int[] days) {
        for (int i = 0; i < days.length; i++) {
            System.out.print(days[i]+" ");
        }
    }
}

